﻿using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Providers
{
    public interface IServiceCollectionProvider
    {
        static IServiceCollection ServiceCollection { get; }
    }
}
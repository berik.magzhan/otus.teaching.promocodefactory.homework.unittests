﻿using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Providers
{
    public sealed class ServiceCollectionProvider : IServiceCollectionProvider
    {
        public static IServiceCollection ServiceCollection { get; private set; }

        public ServiceCollectionProvider(IServiceCollection serviceCollection)
        {
            ServiceCollection = serviceCollection;
        }
    }
}